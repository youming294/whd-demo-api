package com.jmit.demo.project.demo.model;

import lombok.Data;

@Data
public class AreaVO {
    private String areacode;
    private String areaname;
}
