package com.jmit.demo.project.demo.controller;


import com.jmit.demo.common.core.domain.AjaxResult;
import com.jmit.demo.project.demo.mapper.DTSMapper;
import com.jmit.demo.project.demo.mapper.UserMapper;
import com.jmit.demo.project.demo.model.ProvinceVO;
import com.jmit.demo.project.demo.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表单相关
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/demo/api")
public class DemoApiController {
    private static final Logger logger = LoggerFactory.getLogger(DemoApiController.class);

    @Autowired
    private UserMapper userMapper;


    @Autowired
    private DTSMapper dtsMapper;

    // http://127.0.0.1:8080/kt/doc.html
    // http://127.0.0.1:8080/kt/demo/api/getStandardVersion
    //@ApiImplicitParam(paramType = "path",name="id",value = "用户id",required = true)
    @ResponseBody
    @RequestMapping(value = "/getStandardVersion" ,method = {RequestMethod.GET,RequestMethod.POST})
    public String getStandardVersion(){
        String a = "23";


        User user = userMapper.selectByNickName("zxj");
        Map userMap = userMapper.selectByNickNameMap("zxj");

        // var user1 = userMapper.selectById(3);

        Map<String,Object> map1 = new HashMap<>();
        map1.put("nick_name","cq");

        // var user2 = userMapper.selectByMap(map1);
        logger.info(userMap.toString());

        return "1.0.4";

    }


    // http://127.0.0.1:8080/kt/demo/api/queryProvinceList
    @ResponseBody
    @RequestMapping(value = "/queryProvinceList" ,method = {RequestMethod.GET,RequestMethod.POST})
    public AjaxResult queryProvinceList(){


        List<ProvinceVO> provinceVOList = dtsMapper.queryProvinceList();

        return AjaxResult.success("",provinceVOList);
    }


}
