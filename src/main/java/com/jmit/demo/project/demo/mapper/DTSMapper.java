package com.jmit.demo.project.demo.mapper;

import com.jmit.demo.project.demo.model.AreaVO;
import com.jmit.demo.project.demo.model.CityVO;
import com.jmit.demo.project.demo.model.ProvinceVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;


//@DS("slave") //这里是配置数据源注解，默认是master
@Component
public interface DTSMapper {

	// Mapper.xml 方式
	// 三级联动
	List<ProvinceVO> queryProvinceList();
	List<CityVO> queryCityList(@Param("provincecode") String provincecode);
	List<AreaVO> queryAreaList(@Param("citycode") String citycode);

}
