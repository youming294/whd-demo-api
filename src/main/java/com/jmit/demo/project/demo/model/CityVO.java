package com.jmit.demo.project.demo.model;

import lombok.Data;

@Data
public class CityVO {
    private String citycode;
    private String cityname;
}
