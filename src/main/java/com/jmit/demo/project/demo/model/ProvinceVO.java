package com.jmit.demo.project.demo.model;

import lombok.Data;

@Data
public class ProvinceVO {
    private String provincecode;
    private String provincename;
}
