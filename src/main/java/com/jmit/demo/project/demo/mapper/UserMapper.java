package com.jmit.demo.project.demo.mapper;

import com.jmit.demo.project.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.Map;


//@DS("slave") //这里是配置数据源注解，默认是master
@Component
public interface UserMapper {
	// 注解 方式
    /**
     * 注解形式调用存储过程
	 * @param
	 */

    /*@Select("call pro_select_user2(#{map.id,mode=IN,jdbcType=INTEGER},#{map._userName ,mode=OUT,jdbcType=VARCHAR},#{map._id ,mode=OUT,jdbcType=INTEGER})")
    @Options(statementType= StatementType.CALLABLE )
    void proSelectName2(@Param("map") Map map);*/


	/**
	 * 根据keyID查询
	 *
	 * @param nickName 角色ID
	 * @return 列表
	 */
	@Select("select userName,passWord" +
			" from user  " +
			" where nick_name = #{nickName} " +
			" limit 1 ")
	public User selectByNickName(String nickName);


	/**
	 * 根据keyID查询
	 *
	 * @param nickName 角色ID
	 * @return 列表
	 */
	@Select("select userName,passWord" +
			" from user  " +
			" where nick_name = #{nickName} " +
			" limit 1 ")
	public Map selectByNickNameMap(String nickName);

}
