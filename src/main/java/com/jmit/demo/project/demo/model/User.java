package com.jmit.demo.project.demo.model;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("user")
public class User {
    private static final long serialVersionUID = 1L;


    @TableField(value = "id")
    private String id;

    @TableField(value = "userName")
    private String userName;
    @TableField(value = "passWord")
    private String passWord;

    @TableField(value = "user_sex")
    private String userSex;

    @TableField(value = "nick_name")
    private String nickName;

    @TableField(value = "made_time")
    private String madeTime;

}
