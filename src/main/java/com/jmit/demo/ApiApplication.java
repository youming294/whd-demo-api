package com.jmit.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author ruoyi
 */

@SpringBootApplication(exclude= { DataSourceAutoConfiguration.class }) //去掉springboot 默认的数据源配置
@MapperScan("com.jmit.**.mapper") //扫描mapper的包，或者读者可以在对应的mapper上加上@Mapper的注解
public class ApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }

}
